#!/usr/bin/python3

import discord
from discord.ext import commands
import sys,random, asyncio, logging, json, os, aiohttp

logging.basicConfig(filename="discord_bot.log",format='%(asctime)s %(message)s',level=logging.INFO)

TOKEN_FILE = 'config/token'
HELP_FILE = 'config/help'

description = '''Discord bot framework
'''

bot = commands.Bot(command_prefix='.', description=description)
bot.remove_command("help")

logging.basicConfig(level=logging.INFO)

def showhelp(ctx):


    with open(HELP_FILE, "r") as help_file:
        response=help_file.read()
        logging.info("Server " + str(ctx.message.server) + " requested help file!" + "ID: " + str(ctx.message.id))
          
    return response

        
@bot.event
async def on_ready():
    logging.info('Logged in as')
    logging.info(bot.user.name)
    logging.info(bot.user.id)
    logging.info('------')
    logging.info('Bot Starting')

@bot.command(pass_context=True)
async def help(ctx):

    response=showhelp(ctx)
    if response: await bot.say("```" + response + "```")

@bot.command(pass_context=True)
async def ping(ctx):

    await bot.say("```pong```")


with open(TOKEN_FILE, "r") as token_file:

    token_data = json.load(token_file)
    token = token_data["token"]
    bot.run(token)
